provider "aws" {
  access_key = var.aws_access_key
  region     = "us-east-1"
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  s3_force_path_style         = var.localstack_test
  skip_credentials_validation = var.localstack_test
  skip_metadata_api_check     = var.localstack_test
  skip_requesting_account_id  = var.localstack_test

  assume_role {
    role_arn = var.aws_assume_role
  }

  endpoints {
    apigateway     = var.endpoint
    cloudformation = var.endpoint
    cloudwatch     = var.endpoint
    dynamodb       = var.endpoint
    ec2            = var.endpoint
    es             = var.endpoint
    firehose       = var.endpoint
    iam            = var.endpoint
    kinesis        = var.endpoint
    kms            = var.endpoint
    lambda         = var.endpoint
    route53        = var.endpoint
    redshift       = var.endpoint
    s3             = var.endpoint
    secretsmanager = var.endpoint
    ses            = var.endpoint
    sns            = var.endpoint
    sqs            = var.endpoint
    ssm            = var.endpoint
    stepfunctions  = var.endpoint
    sts            = var.endpoint
  }
}
